


//FUNCTIONS




//Function Declaration


/*

	Syntax:
		function functionName() {
				codeblock(statement)
		}


*/

function printName(){
	console.log("My name is John!")
}


//Function Invocation
printName()


//Function Declaration vs Function Expressions


	//Function Declaration


	function declaredFunction() {
		console.log("Hello world form the declaredFunction()")
	}

	declaredFunction()

	//Function Expressions
			// A function can also be stored in a variable. This is called a function expression.

			let variableFunction = function() {
				console.log("Hello from the variableFunction")
			}

			variableFunction();



// Function Scoping

{
	let localVar = "I am a local variable."
}
let globalVar = "I am a global variable"

console.log(globalVar)



	function showNames(){
		//Function scoed variables
		var functionVar = "Joe"
		const functionConst = "John"
		let functionLet = "Jane"
	}


// Nested Functio

	function myNewFunction() {
		let name = "Maria"
		function nestedFunction(){
			let nestedName = "Jose"
			console.log(name)
		}
		nestedFunction()
	}

	myNewFunction()

//Function and Global Scope Variables

	//Global Variables

	let globalName = "Glyn"

	function myNewFunction2(){
		let nameInside = "Jenno"

		console.log(globalName)




	}
	myNewFunction2()

	
// USING ALERT()


	function showSampleAlert() {
		alert("Hello User!")
	}

//	showSampleAlert()

	console.log("Hello Pows!");

// USING PROMPT()

/*	let samplePrompt = prompt("Enter your name: ")
	console.log("Hello, " + samplePrompt)*/



/*	function printWelcomeMessage() {
		let firstname = prompt("Enter your first name: ")
		let lastname = prompt("Enter your last name: ")

		alert("Hello " + firstname + " " + lastname)
		alert("Welcome to my page!")
	}

	printWelcomeMessage()
*/

//Function Naming Conventions

	function displayCarInfo(){
		console.log("Brand: Toyota")
		console.log("Type: Sedan")
		console.log("Price: 1,500,000")
	}

	displayCarInfo();

