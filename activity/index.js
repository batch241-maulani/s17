/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo(){
		let firstName = prompt("What is your name? ")
		let age = prompt("How old are you? ")
		let address = prompt("Where do you live? ")

		console.log("Hello, " + firstName)
		console.log("You are " + age + " years old")
		console.log("You live in " + address)
 
	}
	userInfo()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favouriteBands(){
		console.log("1. Ed Sheeran")
		console.log("2. December Avenue")
		console.log("3. Aimer")
		console.log("4. Yoasobi")
		console.log("5. LiSA")


	}
	favouriteBands()


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favouriteMovies(){
		console.log("1. How to train your dragon 3")
		console.log("Rotten tomator rating: 90%")
		console.log("2. Toy Story 3")
		console.log("Rotten tomator rating: 98%")
		console.log("3. A Silent Voice")
		console.log("Rotten tomator rating: 95%")
		console.log("4. Spirited Away")
		console.log("Rotten tomator rating: 97%")
		console.log("5. Jumanji (1995)")
		console.log("Rotten tomator rating: 52%")
		


	}
	favouriteMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends()